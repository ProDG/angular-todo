export class Item {
  id: number;
  text: string;
  isDeleted: boolean;
}
