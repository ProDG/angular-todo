import { Component, Input, OnInit } from '@angular/core';
import { Item } from '../../item.model';
import { ItemsService } from '../../items.service';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {
  @Input() item: Item;

  constructor(
    public itemsService: ItemsService,
  ) { }

  ngOnInit(): void {
  }

  changeText() {
    this.item.text = 'The text was changed!';
  }

}
