import { Component, Input, OnInit } from '@angular/core';
import { Item } from '../item.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  @Input() itemsList: Item[];

  constructor() { }

  ngOnInit(): void {
  }

}
