import { Injectable } from '@angular/core';
import { Item } from './item.model';

@Injectable({
  providedIn: 'root',
})
export class ItemsService {

  private items: Item[] = [];

  constructor() { }

  getItems() {
    return this.items.slice();
  }

  toggleItemDeleted(itemId: number) {
    for (const item of this.items) {
      if (item.id === itemId) {
        item.isDeleted = !item.isDeleted;
        break;
      }
    }
  }

  deleteItem(itemId: number) {
    const items = [];
    for (const item of this.items) {
      if (item.id !== itemId) {
        items.push(item);
      }
    }
    this.items = items;
  }

  deleteItemCorrect(itemId: number) {
    this.items = this.items.filter(item => item.id !== itemId);
  }

  reset() {
    this.items = [
      {
        id: 1,
        text: 'This is the first item',
        isDeleted: false,
      },
      {
        id: 2,
        text: 'This is the second item',
        isDeleted: false,
      },
      {
        id: 3,
        text: 'This is the deleted item',
        isDeleted: true,
      }
    ];
  }

  clear() {
    this.items = [];
  }

}
